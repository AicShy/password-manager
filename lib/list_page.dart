import 'package:flutter/material.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/services.dart';
import 'package:shared_preferences/shared_preferences.dart';

import 'model.dart';
import "create_model_record.dart";
import 'helper.dart';

class ListPage extends StatefulWidget {
  ListPage({Key? key, this.pinCode, this.firstTime}) : super(key: key);

  String? pinCode;
  final VoidCallback? firstTime;

  @override
  State<ListPage> createState() => _ListPageState();
}

class _ListPageState extends State<ListPage> {
  static const TextStyle style =
      TextStyle(fontWeight: FontWeight.bold, fontSize: 24);
  bool isLoading = true;

  List<Model> array = [];

  @override
  void dispose() {
    super.dispose();
    widget.pinCode = null;
  }

  void copyToClipBoard(String text) {
    Clipboard.setData(ClipboardData(text: text)).then(
        (value) => ScaffoldMessenger.of(context).showSnackBar(const SnackBar(
              content: Text('Скопировано'),
            )));
  }

  Future<void> addToList(Model value) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    List<String> savedList =
        prefs.getStringList(Helper.generateMd5('list')) ?? [];
    setState(() {
      array.add(value);
      array.sort((a, b) => b.date.compareTo(a.date));
    });
    savedList.add(Helper.encrypt(value.toString(), widget.pinCode!));
    prefs.setStringList(Helper.generateMd5('list'), savedList);
  }

  Future<void> removeFromList(Model value) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    List<String> savedList =
        prefs.getStringList(Helper.generateMd5('list')) ?? [];
    setState(() {
      array.remove(value);
      array.sort((a, b) => b.date.compareTo(a.date));
    });
    savedList.remove(Helper.encrypt(value.toString(), widget.pinCode!));
    prefs.setStringList(Helper.generateMd5('list'), savedList);
  }

  void getList() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    List<String> savedList =
        prefs.getStringList(Helper.generateMd5('list')) ?? [];
    if (widget.pinCode != null) {
      for (var element in savedList) {
        setState(() {
          array.add(Model.fromString(Helper.decrypt(element, widget.pinCode!)));
        });
      }
      setState(() {
        array.sort((a, b) => b.date.compareTo(a.date));
      });
    }
    setState(() {
      isLoading = false;
    });
  }

  void addModel() async {
    Navigator.push(
        context,
        MaterialPageRoute(
            builder: (context) => CreateModel(
                pinCode: widget.pinCode!,
                getModel: (Model value) {
                  addToList(value);
                })));
  }

  @override
  void initState() {
    super.initState();

    getList();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      // key: globalKey,
      appBar: AppBar(
        title: const Text("Введите название"),
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: () {
          addModel();
        },
        child: const Icon(Icons.add),
      ),
      body: Container(
        margin: const EdgeInsets.only(left: 10, right: 10, top: 10),
        child: isLoading
            ? const Center(
                child: CircularProgressIndicator(),
              )
            : array.isEmpty
                ? const Padding(
                    padding: EdgeInsets.only(bottom: 50.0),
                    child: Center(
                      child: Text(
                        "Нет записей",
                        style: TextStyle(
                            fontWeight: FontWeight.bold, fontSize: 24),
                        textAlign: TextAlign.center,
                      ),
                    ),
                  )
                : ListView.builder(
                    itemCount: array.length,
                    itemBuilder: (context, index) => Column(
                          children: [
                            index == 0
                                ? Row(
                                    mainAxisAlignment: MainAxisAlignment.start,
                                    children: const [
                                      Text(
                                        "Сайт",
                                        textAlign: TextAlign.center,
                                        style: style,
                                      ),
                                      Expanded(
                                          flex: 1,
                                          child: Text(
                                            "Логин",
                                            style: style,
                                            textAlign: TextAlign.center,
                                          )),
                                      Padding(
                                        padding: EdgeInsets.only(right: 40),
                                        child: Text(
                                          "Пароль",
                                          style: style,
                                          textAlign: TextAlign.center,
                                        ),
                                      )
                                    ],
                                  )
                                : Container(),
                            Row(
                              mainAxisAlignment: MainAxisAlignment.start,
                              children: [
                                Row(
                                  children: [
                                    GestureDetector(
                                      onTap: () {
                                        showDialog(
                                            context: context,
                                            builder: (BuildContext context) {
                                              return AlertDialog(
                                                title: const Text("URL сайта"),
                                                content: GestureDetector(
                                                  child: Text(array[index].url),
                                                  onTap: () {
                                                    copyToClipBoard(
                                                        array[index].url);
                                                  },
                                                ),
                                              );
                                            });
                                      },
                                      child: CachedNetworkImage(
                                        width: 50,
                                        height: 50,
                                        imageUrl: array[index].icon,
                                        placeholder: (context, url) =>
                                            const CircularProgressIndicator(),
                                        errorWidget: (context, url, error) =>
                                            const Icon(Icons.error),
                                      ),
                                      // margin: const EdgeInsets.all(8),
                                    )
                                  ],
                                ),
                                Expanded(
                                  flex: 1,
                                  child: GestureDetector(
                                    onTap: () {
                                      copyToClipBoard(Helper.decrypt(
                                          array[index].login, widget.pinCode!));
                                    },
                                    child: Center(
                                        child: Text(
                                      "•" * 12,
                                      style: const TextStyle(fontSize: 18),
                                    )),
                                  ),
                                ),
                                Row(
                                  mainAxisAlignment:
                                      MainAxisAlignment.spaceBetween,
                                  children: [
                                    GestureDetector(
                                      onTap: () {
                                        copyToClipBoard(Helper.decrypt(
                                            array[index].password,
                                            widget.pinCode!));
                                      },
                                      child: Center(
                                          child: Text(
                                        "•" * 12,
                                        style: const TextStyle(fontSize: 18),
                                      )),
                                    ),
                                    IconButton(
                                        onPressed: () {
                                          showDialog(
                                              context: context,
                                              builder: (BuildContext context) {
                                                return AlertDialog(
                                                  title: const Text("Удаление"),
                                                  content: const Text(
                                                      "Вы точно хотите удалить запись? Отменить это действие невозможно"),
                                                  actions: [
                                                    ElevatedButton(
                                                      onPressed: () {
                                                        Navigator.of(context)
                                                            .pop();
                                                      },
                                                      child: const Text("Нет"),
                                                    ),
                                                    ElevatedButton(
                                                        onPressed: () {
                                                          removeFromList(
                                                              array[index]);
                                                        },
                                                        child: const Text("Да"),
                                                        style: ButtonStyle(
                                                            backgroundColor:
                                                                MaterialStateProperty
                                                                    .all(Colors
                                                                        .red)))
                                                  ],
                                                );
                                              });
                                        },
                                        icon: const Icon(
                                          Icons.close_sharp,
                                          color: Colors.red,
                                        ))
                                  ],
                                )
                              ],
                            ),
                            const Divider()
                          ],
                        )),
      ),
    );
  }
}
