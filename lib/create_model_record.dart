import 'package:favicon/favicon.dart' as favicon;
import 'package:flutter/material.dart';

import 'model.dart';
import 'helper.dart';

class CreateModel extends StatefulWidget {
  const CreateModel({Key? key, required this.pinCode, required this.getModel})
      : super(key: key);

  final String pinCode;

  final Function(Model) getModel;

  @override
  State<CreateModel> createState() => _CreateModelState();
}

class _CreateModelState extends State<CreateModel> {
  final TextEditingController url = TextEditingController();
  final TextEditingController login = TextEditingController();
  final TextEditingController password = TextEditingController();

  bool isLoading = false;
  bool _passwordVisible = false;

  final _formKey = GlobalKey<FormState>();

  Future<String?> loadIcon(String url) async {
    var iconData = await favicon.FaviconFinder.getBest(url);
    if (iconData != null) {
      return iconData.url;
    } else {
      return null;
    }
  }

  Future<void> createModel() async {
    setState(() {
      isLoading = true;
    });
    widget.getModel(Model(
        icon: (await loadIcon(url.text)) ?? "empty",
        url: url.text,
        login: Helper.encrypt(login.text, widget.pinCode),
        password: Helper.encrypt(password.text, widget.pinCode),
        date: DateTime.now()));
    Navigator.pop(context);
    // }
  }

  @override
  void dispose() {
    super.dispose();
    url.dispose();
    login.dispose();
    password.dispose();
  }

  @override
  Widget build(BuildContext context) {
    double height = MediaQuery.of(context).size.height;
    return Scaffold(
      appBar: AppBar(),
      body: Center(
        child: isLoading
            ? const CircularProgressIndicator()
            : Padding(
                padding: const EdgeInsets.all(10.0),
                child: Container(
                  height: height - 365,
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    children: [
                      const Text(
                        "Создание записи",
                        style: TextStyle(
                            fontWeight: FontWeight.bold, fontSize: 24),
                      ),
                      Form(
                        key: _formKey,
                        child: Container(
                          height: height / 2.6,
                          child: Column(
                            mainAxisAlignment: MainAxisAlignment.spaceAround,
                            // crossAxisAlignment: CrossAxisAlignment.center,
                            children: [
                              TextFormField(
                                  controller: url,
                                  decoration: const InputDecoration(
                                    hintText:
                                        "Введите url сайта... (https://example.com)",
                                    border: OutlineInputBorder(
                                      borderRadius:
                                          BorderRadius.all(Radius.circular(15)),
                                    ),
                                  ),
                                  validator: (value) {
                                    if (url.text.isEmpty) {
                                      return "Поле обязательно для заполнения";
                                    } else if (!Uri.parse(url.text)
                                        .isAbsolute) {
                                      return "Данного сайта не существует, или он введет неправильно";
                                    }
                                  }),
                              TextFormField(
                                  controller: login,
                                  decoration: const InputDecoration(
                                    hintText: "Введите логин...",
                                    border: OutlineInputBorder(
                                      borderRadius:
                                          BorderRadius.all(Radius.circular(15)),
                                    ),
                                  ),
                                  validator: (value) {
                                    if (login.text.isEmpty) {
                                      return "Поле обязательно для заполнения";
                                    }
                                  }),
                              TextFormField(
                                  controller: password,
                                  obscureText: !_passwordVisible,
                                  obscuringCharacter: "•",
                                  decoration: InputDecoration(
                                      hintText: "Введите пароль...",
                                      border: const OutlineInputBorder(
                                        borderRadius: BorderRadius.all(
                                            Radius.circular(15)),
                                      ),
                                      suffixIcon: IconButton(
                                          onPressed: () {
                                            setState(() {
                                              _passwordVisible =
                                                  !_passwordVisible;
                                            });
                                          },
                                          icon: Icon(_passwordVisible
                                              ? Icons.visibility
                                              : Icons.visibility_off))),
                                  validator: (value) {
                                    if (password.text.isEmpty) {
                                      return "Поле обязательно для заполнения";
                                    }
                                  }),
                              ElevatedButton(
                                onPressed: () {
                                  if (_formKey.currentState!.validate()) {
                                    createModel();
                                  }
                                },
                                child: const Text(
                                  "Создать запись",
                                  style: TextStyle(fontWeight: FontWeight.bold),
                                ),
                              )
                            ],
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
              ),
      ),
    );
  }
}
