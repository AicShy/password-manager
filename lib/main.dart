import 'package:flutter/material.dart';

import 'package:shared_preferences/shared_preferences.dart';

import 'helper.dart';
import 'enter_pincode_page.dart';
import 'create_pincode_page.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  SharedPreferences prefs = await SharedPreferences.getInstance();
  Widget? home;

  if (prefs.getStringList(Helper.generateMd5('list')) != null &&
      prefs.getStringList(Helper.generateMd5('list'))!.isNotEmpty) {
    home = const EnterPincode();
  } else {
    home = const CreatePincode();
  }
  runApp(MaterialApp(
    home: home,
    debugShowCheckedModeBanner: false,
  ));
}
