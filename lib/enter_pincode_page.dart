import 'package:shared_preferences/shared_preferences.dart';
import 'package:flutter/material.dart';

import 'helper.dart';
import 'list_page.dart';

class EnterPincode extends StatefulWidget {
  const EnterPincode({Key? key}) : super(key: key);

  @override
  State<EnterPincode> createState() => _EnterPincodeState();
}

class _EnterPincodeState extends State<EnterPincode> {
  final TextEditingController controller = TextEditingController();
  @override
  void initState() {
    super.initState();

    getList();
  }

  bool _passwordVisible = false;
  final _formKey = GlobalKey<FormState>();
  List<String> savedList = [];

  Future<void> getList() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    List<String> list = prefs.getStringList(Helper.generateMd5('list'))!;
    setState(() {
      savedList = list;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Padding(
        padding: const EdgeInsets.all(10.0),
        child: Center(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: [
              const Text(
                "Введите пин-код",
                style: TextStyle(fontWeight: FontWeight.bold, fontSize: 24),
              ),
              Form(
                key: _formKey,
                child: TextFormField(
                    controller: controller,
                    obscureText: !_passwordVisible,
                    obscuringCharacter: "•",
                    decoration: InputDecoration(
                        hintText: "Введите пароль...",
                        border: const OutlineInputBorder(
                          borderRadius: BorderRadius.all(Radius.circular(15)),
                        ),
                        suffixIcon: IconButton(
                            onPressed: () {
                              setState(() {
                                _passwordVisible = !_passwordVisible;
                              });
                            },
                            icon: Icon(_passwordVisible
                                ? Icons.visibility
                                : Icons.visibility_off))),
                    validator: (value) {
                      if (controller.text.isEmpty) {
                        return "Поле пинкода не может быть пустым";
                      } else {
                        try {
                          print(Helper.decrypt(savedList.first,
                              Helper.generateMd5(controller.text)));
                        } catch (e) {
                          return "Неверный пинкод";
                        }
                      }
                    }),
              ),
              ElevatedButton(
                onPressed: () {
                  if (_formKey.currentState!.validate()) {
                    Navigator.pushReplacement(
                        context,
                        MaterialPageRoute(
                            builder: (context) => ListPage(
                                  pinCode: Helper.generateMd5(controller.text),
                                )));
                  }
                },
                child: const Text(
                  "Вход",
                  style: TextStyle(fontWeight: FontWeight.bold),
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
}
