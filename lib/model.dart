class Model {
  String icon = "empty";
  String url = "";
  String login = "";
  String password = "";
  DateTime date = DateTime.now();

  @override
  String toString() {
    return '$icon\$$url\$$login\$$password\$$date';
  }

  Model(
      {required this.icon,
      required this.url,
      required this.login,
      required this.password,
      required this.date});

  Model.fromString(String value) {
    List<String> array = value.split('\$');
    icon = array[0];
    url = array[1];
    login = array[2];
    password = array[3];
    date = DateTime.parse(array[4]);
  }
}
