import 'package:flutter/material.dart';
import 'helper.dart';
import "list_page.dart";

class CreatePincode extends StatefulWidget {
  const CreatePincode({
    Key? key,
  }) : super(key: key);

  @override
  State<CreatePincode> createState() => _CreatePincodeState();
}

class _CreatePincodeState extends State<CreatePincode> {
  final TextEditingController controller = TextEditingController();

  bool _passwordVisible = false;

  final _formKey = GlobalKey<FormState>();

  @override
  void dispose() {
    super.dispose();
    controller.dispose();
  }

  @override
  Widget build(BuildContext context) {
    double height = MediaQuery.of(context).size.height;
    return Scaffold(
      appBar: AppBar(),
      body: Center(
        child: Container(
          height: height - 365,
          child: Padding(
            padding: const EdgeInsets.all(10.0),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                const Padding(
                  padding: EdgeInsets.all(8.0),
                  child: Text(
                    "Добро пожаловать!",
                    style: TextStyle(fontWeight: FontWeight.bold, fontSize: 24),
                    textAlign: TextAlign.center,
                  ),
                ),
                const Padding(
                  padding: EdgeInsets.only(top: 8, bottom: 25),
                  child: Text(
                    "Создайте пароль для входа в приложение.",
                    textAlign: TextAlign.center,
                    style: TextStyle(fontSize: 18),
                  ),
                ),
                const Padding(
                  padding: EdgeInsets.only(bottom: 25),
                  child: Text(
                      'Пароль не будет сохранен, пока не будет создана первая запись',
                      textAlign: TextAlign.center,
                      style: TextStyle(color: (Colors.red), fontSize: 18)),
                ),
                Form(
                  key: _formKey,
                  child: Column(
                    children: [
                      TextFormField(
                        obscureText: !_passwordVisible,
                        obscuringCharacter: "•",
                        decoration: InputDecoration(
                            border: const OutlineInputBorder(
                              borderRadius:
                                  BorderRadius.all(Radius.circular(15)),
                            ),
                            suffixIcon: IconButton(
                                onPressed: () {
                                  setState(() {
                                    _passwordVisible = !_passwordVisible;
                                  });
                                },
                                icon: Icon(_passwordVisible
                                    ? Icons.visibility
                                    : Icons.visibility_off))),
                        controller: controller,
                        validator: (value) {
                          if (value == null ||
                              value.isEmpty ||
                              value.length < 4) {
                            return 'Минимальная длина пароля составляет 4 символа';
                          }
                          return null;
                        },
                      ),
                      Padding(
                        padding: const EdgeInsets.all(16.0),
                        child: ElevatedButton(
                          onPressed: () {
                            if (_formKey.currentState!.validate()) {
                              final hashedPincode =
                                  Helper.generateMd5(controller.text);
                              Navigator.pushReplacement(
                                  context,
                                  MaterialPageRoute(
                                      builder: (context) =>
                                          ListPage(pinCode: hashedPincode)));
                            }
                          },
                          child: const Text('Создать'),
                        ),
                      ),
                    ],
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
