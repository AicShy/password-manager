import 'dart:convert';

import 'package:crypto/crypto.dart';
import 'package:encrypt/encrypt.dart';

class Helper {
  static String generateMd5(String input) {
    return md5.convert(utf8.encode(input)).toString();
  }

  static String encrypt(String text, String password) {
    final key = Key.fromUtf8(generateMd5(password));
    final iv = IV.fromLength(16);
    final encrypter = Encrypter(AES(key));
    final encrypted = encrypter.encrypt(text, iv: iv);
    return encrypted.base64;
  }

  static String decrypt(String text, String password) {
    final key = Key.fromUtf8(generateMd5(password));
    final iv = IV.fromLength(16);
    final encrypter = Encrypter(AES(key));
    final encrypted = Encrypted.fromBase64(text);
    final content = encrypter.decrypt(encrypted, iv: iv);
    return content;
  }
}
